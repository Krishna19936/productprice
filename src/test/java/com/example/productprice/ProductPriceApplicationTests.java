package com.example.productprice;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.example.productprice.Service.ProductPriceService;
import com.example.productprice.entity.ProductPrice;
import com.example.productprice.exception.ProductNotFoundException;
import com.example.productprice.repository.ProductPriceRepository;

@SpringBootTest
class ProductPriceApplicationTests {
	
	@Autowired
	private ProductPriceService productPriceService;
	
	@MockBean
	private ProductPriceRepository productPriceRepository;
	
	public void getProductPricebyIdTest() throws ProductNotFoundException {
		
		List<ProductPrice> list = new ArrayList<>();
		ProductPrice pp = new ProductPrice(1,10);
		list.add(pp);
		
		Mockito.when(productPriceRepository.findById(1))
		.thenReturn(Optional.of(pp));
		assertEquals(pp, productPriceService.getProductPricebyId(1));
	}


}
