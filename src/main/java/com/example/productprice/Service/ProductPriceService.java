package com.example.productprice.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.productprice.entity.ProductPrice;
import com.example.productprice.exception.ProductNotFoundException;
import com.example.productprice.repository.ProductPriceRepository;

@Service
public class ProductPriceService {
	
	@Autowired
	private ProductPriceRepository productPriceRepository;
	
	public ProductPrice getProductPricebyId(int productId) throws ProductNotFoundException{
		//LOGGER.info("MovieService || getMovieById()");
		if(!(productPriceRepository.findById(productId)).isPresent())
		{
			
		throw new ProductNotFoundException();
		
		}
		else {
		return productPriceRepository.findById(productId).orElse(null);
		}
		
	}

}
