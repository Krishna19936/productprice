package com.example.productprice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.productprice.entity.ProductPrice;


public interface ProductPriceRepository extends JpaRepository<ProductPrice,Integer> {

	
	
	

}
