package com.example.productprice.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.productprice.Service.ProductPriceService;
import com.example.productprice.entity.ProductPrice;
import com.example.productprice.exception.ProductNotFoundException;
import com.google.common.base.Optional;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/")
@Api(value = "EcommerceController", description = "API for Ecommerce app")
public class ProductPriceController {
	
private static final Logger LOGGER = LoggerFactory.getLogger(ProductPriceController.class);
	
	@Autowired
	private ProductPriceService productPriceService;

	
    @GetMapping("/productprice/{productId}")
    public ResponseEntity<ProductPrice> findProductPriceById(@PathVariable int productId) throws ProductNotFoundException  {
	LOGGER.debug("ProductPriceController | findProductPriceById()");
        return new ResponseEntity<>(productPriceService.getProductPricebyId(productId), HttpStatus.OK);
        

}
}
