package com.example.productprice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ProductPriceMasterExceptionHandler {
	
	 @ExceptionHandler(value = ProductNotFoundException.class)
	   public ResponseEntity<Object> handleProductNotFoundException() {
		   return new ResponseEntity<>("Product does not exist in our system", HttpStatus.NOT_FOUND);
	}

}
