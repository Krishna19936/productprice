package com.example.productprice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUCT_PRICE")
public class ProductPrice {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private int productId;

	 

	    @Column(name = "product_price")
	    private int productPrice;
	    
	    

	    public ProductPrice() {
	        super();
	        // TODO Auto-generated constructor stub
	    }
	    
	    public ProductPrice(int productId, int productPrice) {
	        super();
	        this.productId = productId;
	        this.productPrice = productPrice;
	    }



		public int getProductId() {
			return productId;
		}



		public void setProductId(int productId) {
			this.productId = productId;
		}



		public int getProductPrice() {
			return productPrice;
		}



		public void setProductPrice(int productPrice) {
			this.productPrice = productPrice;
		}
}
